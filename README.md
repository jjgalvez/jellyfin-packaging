# jellyfin-packaging

Jelyfin split PKGBUILD for jellyfin-server and jellyfin-web to upgrade to 10.9.2

The current packaging for jellyfin has changed significantly from 10.8.x and had moved to jellyfin-packaging. Also they no longer build RPM packages and currently only build for debian lts. This will requires a significant change to how the packages are build for Arch.

This is a temporary package for anyone like myself who is impatient. It is heavily influcned (most of the PKGBUILD file is taken direclty from the Arch srouce) by the PKGBUILD files in [Extra] and I am an sure we will see official packages soon.

If you use this to PKGBUILD file to build and install jellyfin-server and jellyfin-web do so at your own risk, if it blows up your server, thats on you, but I can say it's working fine on my mediaserver.


## Project status
temporary project needed only as long as it takes for official packages to hit [Extra]
