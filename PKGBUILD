# Maintainer:

pkgname=(jellyfin-server jellyfin-web)
pkgbase=jellyfin-packaging
_pkgname="jellyfin"
_server_pkgname="${_pkgname}-server"
_web_pkgname="${_pkgname}-web"
pkgver=10.9.3
pkgrel=1
pkgdesc='Jellyfin server backend and web frontend'
arch=('x86_64')
url='https://jellyfin.org'
_url='https://github.com/jellyfin/jellyfin-packaging'
license=('GPL2')
_dotnet_ver=8.0
_dotnet_runtime=linux-x64
depends=(
  "aspnet-runtime-$_dotnet_ver"
  'bash'
  'sqlite'
  'fontconfig'
  'jellyfin-ffmpeg'
)
makedepends=(
  'git'
  'python'
  'python-gitpython'
  "dotnet-sdk-$_dotnet_ver"
  'nodejs'
  'npm'
)
optdepends=()
backup=(
  "etc/$_pkgname/logging.json"
  "etc/$_pkgname/$_pkgname.env"
)
options=('!debug')
_commit='f368a7c998d202aa116c5fa5800048f2858829fa'
source=(
  "$pkgbase::git+$_url#commit=$_commit"
  "fix-envfile-path.patch"
  "fix-jellyfin-web-path.patch"
  "tmpfiles.conf"
  "sysusers.conf"
  'jellyfin-firewalld.xml'
)

b2sums=('87352f7e976fb54528e2c921b3a867de86da50720f72102b782732efe75544be7736d3a2f4a2050dda70ffa6da86d21c4a8567e860be0a4344dc25e07ba3d4bd'
        '4f63c425fc60e80c36c958674f0bbd704feb79cac9e8d05e76e0bc01d6d3e10555395a45f994e240a68433c01f9a14d5a1db1765eccb7409f11da34161c75ffc'
        '1991c9a95ae6d5263024b5c0ba0cb8ab5b63fad9366d956bb627a15df1ea0ef9ec3250a46d72cb0bc534674a7bc89614043f9a6b301ce7bcaef7e6c0ed751a99'
        'bc8001cf28ebf76a125e7ab0d9d5a8fcf35e0def5b907dc5fe90e16cdbb3fdf8b7f098779ced9a44e7a3918ee605058b379d445a224178456a32a6a99cd084b4'
        'f73c72e1d66480c410dbe71be1af91a2bd76206dfed76e64b8264ee3537f6eb477bcba905ce69a0ec8b273e33c2e46439b9b32ce21543a48d98d80434a46a781'
        'eb1b0c959c7392ffc6342b55ff65301c8363a5f5921107b339e3560aada873e2f8a52d1d47d441de3440f945e90cd67fcae2160d9fa2ce7827dc814f215a3fd7')

prepare() {
  cd $pkgbase
  python checkout.py v${pkgver}

  prepare_jellyfin-server
  prepare_jellyfin-web
}

build() {
  build_jellyfin-server
  build_jellyfin-web
}

prepare_jellyfin-server() {
  cd "${srcdir}/${pkgbase}"

  # fix envfile path in systemd service
  patch -p1 -i "$srcdir/fix-envfile-path.patch"
  patch -p1 -i "$srcdir/fix-jellyfin-web-path.patch"

}

prepare_jellyfin-web() {
  cd "${srcdir}/${pkgbase}/${_web_pkgname}"

  # download dependencies
  # FS#79713 - remove environment variable with 10.9.x release
  SKIP_PREPARE=1 npm ci --no-audit --no-fund --no-update-notifier
}

build_jellyfin-server() {

  cd "${srcdir}/${pkgbase}/${_server_pkgname}"

  # disable dotnet telemetry
  export DOTNET_CLI_TELEMETRY_OPTOUT=1
  export DOTNET_SKIP_FIRST_TIME_EXPERIENCE=1
  export DOTNET_NOLOGO=1

  # force dotnet to use 6.0 (incompatible with 6.0+…)
  dotnet new globaljson --sdk-version "$_dotnet_ver" --roll-forward latestMinor --force

  dotnet \
    publish \
    Jellyfin.Server \
    --configuration Release \
    --output builddir \
    --self-contained false\
    --runtime "$_dotnet_runtime" \
    -p:DebugSymbols=false \
    -p:DebugType=none

}

build_jellyfin-web() {

  pkgdesc='Web client for Jellyfin'
  depends=()

  cd "${srcdir}/${pkgbase}/${_web_pkgname}"

  npm run build:production
}

package_jellyfin-server() {

  pkgdesc="Jellyfin server backend"
  depends=(
    "aspnet-runtime-$_dotnet_ver"
    'bash'
    'sqlite'
    'fontconfig'
    'jellyfin-ffmpeg'
  )

  cd "${srcdir}/${pkgbase}/${_server_pkgname}"

  # install binaries
  install -vd "$pkgdir/usr/"{lib,bin}
  cp -r builddir "$pkgdir/usr/lib/jellyfin"
  ln -sf /usr/lib/jellyfin/jellyfin "$pkgdir/usr/bin/jellyfin"

  # ensure binaries have correct permissions
  chmod 755 "$pkgdir/usr/lib/$_pkgname/jellyfin"

  # logging configuration
  install -vdm 750 "$pkgdir/etc/$_pkgname"
  install -vDm640 \
    Jellyfin.Server/Resources/Configuration/logging.json \
    -t "$pkgdir/etc/$_pkgname"

  cd "${srcdir}/${pkgbase}/debian"

  # systemd integration
  install -vDm644 conf/jellyfin.service -t "$pkgdir/usr/lib/systemd/system"
  install -vDm644 "$srcdir/sysusers.conf" "$pkgdir/usr/lib/sysusers.d/$_pkgname.conf"
  install -vDm644 "$srcdir/tmpfiles.conf" "$pkgdir/usr/lib/tmpfiles.d/$_pkgname.conf"
  install -vDm640 conf/jellyfin "$pkgdir/etc/$_pkgname/$_pkgname.env"

  # firewalld configuration
  install -vDm644 ${srcdir}/jellyfin-firewalld.xml "$pkgdir/usr/lib/firewalld/services/$_pkgname.xml"

}

package_jellyfin-web() {
  cd "${pkgbase}/${_web_pkgname}"

  install -vd "$pkgdir/usr/share/$_web_pkgname"
  cp -vr dist/* "$pkgdir/usr/share/$_web_pkgname"
}
